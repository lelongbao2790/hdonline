//
//  AppDelegate.h
//  HDOnline
//
//  Created by Bao (Brian) L. LE on 5/5/16.
//  Copyright © 2016 LongBao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ListCategoryController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

+ (AppDelegate *)share;

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) JASidePanelController *mainPanel;
@property (strong, nonatomic) MainController *mainController;
@property (strong, nonatomic) ListCategoryController *listCategoryController;
@end

