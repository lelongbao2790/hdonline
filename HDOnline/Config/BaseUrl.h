//
//  BaseUrl.h
//  HDOnline
//
//  Created by Bao (Brian) L. LE on 5/6/16.
//  Copyright © 2016 LongBao. All rights reserved.
//

#ifndef BaseUrl_h
#define BaseUrl_h

// Base Url
#define kBaseMobileUrl  @"http://api.hdonline.vn/v2/"
#define kBaseWebUrl @"http://hdonline.vn/"
#define kBaseImageUrl @"http://hdonline.vn/i/resources/new/%@"

// List API
#define kUrlPhimLe @"film/list/type/single?limit=20&offset=%d&token=%@"
#define kUrlPhimBo @"film/list/type/drama?limit=20&offset=%d&token=%@"
#define kUrlHDODeCu @"film/list/type/votehome?limit=20&offset=%d&token=%@"
#define kUrlTopHDO  @"film/list/type/topmovies?limit=20&offset=%d&token=%@"
#define kUrlGetToken @"index/gettoken?client=tablet&clientsecret=eba71161466442ff1733fd0c022ac957&device=tablet&platform=ios&version=1.0"
#define kUrlLogin @"user/login?token=%@&username=%@&password=%@"
#define kUrlPlayPhimLe @"episode/play?id=%@&token=%@"
#define kUrlPlayPhimBo @"episode?film=%@&token=%@"
#define kUrlGetIdMovie @"episode?film=%@&token=%@"
#define kUrlSearchMovie @"suggestsearch.html?q=%@"
#define kUrlSearchMobile @"film/list/type/search?search=%@&token=%@"
#define kUrlPhimChieuRap @"film/list/type/cinema?limit=20&offset=%d&token=%@"
#define kUrlGetCategory @"category?offset=%d&token=%@"
#define kUrlCountry @"country?offset=%d&token=%@"
#define kUrlListMovieCategoryByYear @"film/list/type/category?category=%@&year=%@&token=%@"
#define kUrlListMovieCountryByYear @"film/list/type/country?country=%@&year=%@&token=%@"
#define kUrlListMovieCategory @"film/list/type/category?category=%@&limit=20&offset=%d&token=%@"
#define kUrlListMovieCountry @"film/list/type/country?country=%@&limit=20&offset=%d&token=%@"
#endif /* BaseUrl_h */
