//
//  CommonSize.h
//  HDOnline
//
//  Created by Bao (Brian) L. LE on 5/6/16.
//  Copyright © 2016 LongBao. All rights reserved.
//

#ifndef CommonSize_h
#define CommonSize_h

#define sizeCellFilmIp5 CGSizeMake(155, 228)
#define sizeCellFilmIp6 CGSizeMake(183, 218)
#define sizeCellFilmIp6Plus CGSizeMake(203, 288)

#endif /* CommonSize_h */
