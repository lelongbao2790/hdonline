//
//  CommonString.h
//  HDOnline
//
//  Created by Bao (Brian) L. LE on 5/6/16.
//  Copyright © 2016 LongBao. All rights reserved.
//

#ifndef CommonString_h
#define CommonString_h

// Login String
#define kLoginFail @"Đăng nhập thất bại"
#define kLoginSuccess @"Đăng nhập thành công"
#define kInputUsername @"Nhập username và password"
#define kEmptyString @""
#define kLoading @"Loading"
#define kDetailCellIdentifier @"DetailMovieCellIdentifier"
#define kTokenExpire @"Phiên đăng nhập không hợp lệ! Login để tiếp tục sử dụng"
#define kErrors @"Lỗi"
#define kOK @"OK"
#define kLogOut @"-> Đăng xuất"

// Data for left menu
#define kName @"name"
#define kLevel @"level"
#define kObjects @"Objects"
#define kBaseUrlDict @"baseurl"

// Movie
#define kNoInformation @"Chưa có thông tin";
#define kErrorPlayMovie @"Lỗi! Link film bị lỗi"
#define kChoiceEpiMovie @"Chọn tập phim và chọn định dạng phim muốn xem"
#define kAlertTitle @"Thông báo"
#define kNoLink @"Phim này hiện chưa có link"
#define kChoiceQuality @"Chọn định dạng phim muốn xem."

// Crash
#define kConsoleLog @"console.log"
#define kCrashTitle @"Crash Log"
#define kEmailSendCrash @"lelongbao2790@gmail.com"

/**
 * Watch
 */
#define kWatchMovieFromStart @"Xem lại"

/**
 * Watch
 */
#define kWatchMovieFromTime @"Xem tiếp"
/**
 * Message for time play
 */
#define kMessageForTimePlay @"Bạn đã xem phim %@ tới phút %@, bạn muốn xem tiếp theo hay xem lại từ đầu."

/**
 * Message time play for epi
 */
#define kMessageTimePlayForEpi @"Bạn đã xem phim %@ tập %d tới phút %@, bạn muốn xem tiếp theo hay xem lại từ đầu."

#endif /* CommonString_h */
