//
//  Enumeration.h
//  HDOnline
//
//  Created by Bao (Brian) L. LE on 5/6/16.
//  Copyright © 2016 LongBao. All rights reserved.
//

#ifndef Enumeration_h
#define Enumeration_h

typedef NS_ENUM(NSInteger, ServerResponseKey) {
    kRequestSuccess = 0,
    kRequestFail = 1,
};

typedef NS_ENUM(NSInteger, FooterCollection) {

    kSpaceLoading = 35,
    kSpaceLoadingX = 80,
    kWidthLabelLoadMore = 150,
};

typedef NS_ENUM(NSInteger, Page) {
    
    kPagePlus = 20,
};

typedef NS_ENUM(NSInteger, TagMainController) {
    kTagMPMoviePlayerController = 1004,
};

typedef NS_ENUM(NSInteger, MainPageTag) {
    
    kTagCategory = 100,
    kTagCountry = 101,
    kTagYear = 102,
};

#endif /* Enumeration_h */
