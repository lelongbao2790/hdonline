//
//  KeyServer.h
//  HDOnline
//
//  Created by Bao (Brian) L. LE on 5/6/16.
//  Copyright © 2016 LongBao. All rights reserved.
//

#ifndef KeyServer_h
#define KeyServer_h

// JSON Response Login
#define kError @"error"
#define kResult @"result"
#define kMessage @"msg"
#define kUser @"user"
#define kToken @"token"
#define kPassword @"password"
#define kId @"id"
#define kOrder @"order"
#define kUserName @"username"
#define KEmail @"email"
#define kName @"name"
#define kImage @"image"

// JSON Response Movie
#define kNameVN @"name_vn"
#define kNameEN @"name_en"
#define kYear @"year"
#define kInfor @"info"
#define kTotalEpisode @"total_episode"
#define kIMDB @"imdb"
#define kUpdateEpisode @"update_episode"
#define kImageList @"imagelist"
#define kFilmRate @"film_rate"
#define kSequence @"sequence"
#define k215_215 @"215_215"
#define k215_311 @"215_311"
#define k455_215 @"455_215"
#define K900_500 @"900_500"
#define kTotal @"total"
#define kPage @"page"
#define kCurrent @"current"
#define kOffset @"offset"
#define kCategory @"category"
#define kInfor @"info"
#define kTrailer @"trailer"
#define kSubtitle @"subtitle"
#define kLevel @"level"
#define kFile @"file"
#define kCode @"code"
#define kSubVN @"vi"
#define kSubEN @"en"
#define kLink720 @"720"
#define kLink480 @"480"
#define kLink360 @"360"
#define kLabel @"label"
#define kDetailBaseUrl @"detailBaseUrl"
#define kLinkPlay @"link"

// Header Web Request
#define kApplicationJson @"application/json"
#define kContentType @"Content-Type"

// Error
#define k400BadRequest @"Request failed: bad request (400)"
#define k502BadRequest @"Request failed: bad gateway (502)"
#define k500BadRequest @"Request failed: internal server error (500)"
#define kServerOverLoad @"Server quá tải"

// Search
#define kDatePublish @"datepublish"
#define kThumb @"thumb"
#define kDocs @"docs"
#define kThumb215215 @"7"
#define kThumb215311 @"5"
#define kThumb455215 @"6"
#endif /* KeyServer_h */
