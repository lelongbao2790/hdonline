
//
//  StoryBoardID.h
//  HDOnline
//
//  Created by Bao (Brian) L. LE on 5/6/16.
//  Copyright © 2016 LongBao. All rights reserved.
//

#ifndef StoryBoardID_h
#define StoryBoardID_h

#define kMainControllerStoryBoardID @"MainController"
#define kLeftMenuControllerStoryBoardID @"LeftMenuController"
#define kPlayControllerStoryBoardID @"PlayController"
#define kSearchCellIdentifier @"SearchCellIdentifier"
#define kSearchCell @"SearchCell"
#define kPlayerCellIdentifier @"PlayerCellIdentifier"
#define kLoginControllerStoryBoardID @"LoginController"
#define kLeftMenuCellIdentifier @"LeftMenuCellIdentifier"
#define kListCategoryController @"ListCategoryController"

#endif /* StoryBoardID_h */
