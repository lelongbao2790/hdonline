//
//  ListCategoryController.h
//  HDOnline
//
//  Created by Bao (Brian) L. LE on 5/16/16.
//  Copyright © 2016 LongBao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListCategoryController : UIViewController
@property (strong, nonatomic) NSString *baseUrl;
@property (strong, nonatomic) NSString *urlRequest;

@end
