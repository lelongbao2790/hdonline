//
//  ListCategoryController.m
//  HDOnline
//
//  Created by Bao (Brian) L. LE on 5/16/16.
//  Copyright © 2016 LongBao. All rights reserved.
//

#import "ListCategoryController.h"

@interface ListCategoryController () <UITableViewDataSource, UITableViewDelegate, GetListCategoryDelegate>

@property (strong, nonatomic) NSMutableArray *listCategory;
@property (weak, nonatomic) IBOutlet UITableView *tbvListCategory;
@property (strong, nonatomic) LeftMenuController *leftMenu;
@end

@implementation ListCategoryController

# pragma Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.listCategory = [[NSMutableArray alloc] init];
    [AppDelegate share].listCategoryController = self;
    self.tbvListCategory.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tbvListCategory.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    self.leftMenu = (LeftMenuController *)[AppDelegate share].mainPanel.leftPanel;
}

- (void)viewWillAppear:(BOOL)animated {
    [DataManager shared].getListCategoryDelegate = self;
    self.tbvListCategory.delegate = self;
    self.tbvListCategory.dataSource = self;
    [self requestUrl];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

# pragma Table View Delegate
// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.listCategory count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
    }
    
    Category *category = self.listCategory[indexPath.row];
    cell.textLabel.text= category.name;
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    cell.textLabel.textColor = kBrownColor;
    cell.backgroundColor = kDefaultColor;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{ 
    Category *category = self.listCategory[indexPath.row];
    
    self.urlRequest = [self.urlRequest stringByReplacingOccurrencesOfString:@"ry=%@" withString:[NSString stringWithFormat:@"ry=%@",category.idCategory]];
    
    if ([self.leftMenu.delegate respondsToSelector:@selector(leftMenuController:withUrl:andOffset:)]){
        [self.leftMenu.delegate leftMenuController:self.leftMenu withUrl:self.urlRequest andOffset:0];
    }
    
    [self.navigationController pushViewController:[AppDelegate share].mainController animated:YES];
    
}



- (void)requestUrl {
    [self.listCategory removeAllObjects];
    [self.tbvListCategory reloadData];
    ProgressBarShowLoading(kLoading);
    NSString *urlRequest = [NSString stringWithFormat:self.baseUrl, 0,[User share].accessToken];
    [[DataManager shared] getListCategoryFromUrl:urlRequest];
}

#pragma Get List Category Delegate
- (void)getListCategoryAPISuccess:(NSArray *)response {
    ProgressBarDismissLoading(kEmptyString);
    
    
    for (NSDictionary *dictResponse in response) {
        Category *newCategory = [Category initCategoryFromJSON:dictResponse];
        [self.listCategory addObject:newCategory];
    }
    
    [self.tbvListCategory reloadData];

}

- (void)getListCategoryAPIFail:(NSString *)resultMessage {
    ProgressBarDismissLoading(kEmptyString);
}


@end
