//
//  DetailMovieCell.h
//  HDOnline
//
//  Created by Bao (Brian) L. LE on 5/9/16.
//  Copyright © 2016 LongBao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Movie.h"

@interface DetailMovieCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgPoster;
@property (weak, nonatomic) IBOutlet UILabel *lbNameMovie;
@property (weak, nonatomic) IBOutlet UILabel *lbEpisode;

- (void) loadInformationWithMovie:(Movie *)movie;

@end
