//
//  DetailMovieCell.m
//  HDOnline
//
//  Created by Bao (Brian) L. LE on 5/9/16.
//  Copyright © 2016 LongBao. All rights reserved.
//

#import "DetailMovieCell.h"

@implementation DetailMovieCell

- (void) loadInformationWithMovie:(Movie *)movie {
    self.lbNameMovie.text = movie.nameVN;
    NSURLRequest *imageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:movie.poster]
                                                  cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                              timeoutInterval:60];
    
    
    [self.imgPoster setImageWithURLRequest:imageRequest
                           placeholderImage:[UIImage imageNamed:kImageDefault]
                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                        self.imgPoster.image = image;
                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                                        self.imgPoster.image = [UIImage imageNamed:kImageDefault];
                                    }];
    
    // Check episode
    if ([movie.updateEpisode isEqualToString:kEmptyString]) {
        self.lbEpisode.hidden = YES;
    } else {
        self.lbEpisode.hidden = NO;
        self.lbEpisode.text = [NSString stringWithFormat:@"Tập %@", movie.updateEpisode];
    }

    self.lbEpisode.layer.cornerRadius = self.lbEpisode.frame.size.width /2;
    self.lbEpisode.layer.masksToBounds = YES;
    
    self.imgPoster.layer.cornerRadius = 4.0;
    self.imgPoster.layer.masksToBounds = YES;
}

@end
