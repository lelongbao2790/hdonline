//
//  ListMovieController.h
//  HDOnline
//
//  Created by Bao (Brian) L. LE on 5/6/16.
//  Copyright © 2016 LongBao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListMovieController : UIViewController

@property (nonatomic, strong) NSMutableArray *listMovie;


@end
