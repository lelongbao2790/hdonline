//
//  ListMovieController.m
//  HDOnline
//
//  Created by Bao (Brian) L. LE on 5/6/16.
//  Copyright © 2016 LongBao. All rights reserved.
//

#import "ListMovieController.h"
#define kHeaderCollectionView @"header"
#define kFooterCollectionView @"footer"
#define kLoadMoreData @"Load More Data"

@interface ListMovieController ()<UICollectionViewDelegate, UICollectionViewDataSource, GetMovieDelegate, LeftMenuDelegate>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionMovie;
@property (strong, nonatomic) UIActivityIndicatorView *loading;
@property (strong, nonatomic) UILabel *loadingLabel;
@property (assign, nonatomic) NSInteger currentPage;
@property (assign, nonatomic) NSInteger totalMovie;
@property (assign, nonatomic) NSInteger offsetMovie;
@property (strong, nonatomic) NSString *urlRequest;

@end

@implementation ListMovieController

# pragma Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.listMovie = [[NSMutableArray alloc] init];
    [self loadMovieFirstTime];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [DataManager shared].getMovieDelegate = self;
    LeftMenuController *leftMenu = (LeftMenuController *)[AppDelegate share].mainPanel.leftPanel;
    leftMenu.delegate = self;
}

# pragma Helper Method
- (void)requestTopMovie:(NSString *)url {
    [[DataManager shared] getMovieFromUrl:url];
}

- (void)loadMovieFirstTime {
    
    self.urlRequest = kUrlTopHDO;
    self.currentPage = 0;
    self.offsetMovie = 0;
    
    ProgressBarShowLoading(kLoading);
    [[DataManager shared] getTokenWithUrl:kUrlGetToken];
}

# pragma UICollection Delegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.listMovie.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    // Configure the cell
    DetailMovieCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kDetailCellIdentifier forIndexPath:indexPath];
    [cell loadInformationWithMovie:self.listMovie[indexPath.row]];
    
    if(indexPath.item == (self.listMovie.count - 1)) {
        [self checkListMovie];
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    PlayController *playController = InitStoryBoardWithIdentifier(kPlayControllerStoryBoardID);
    playController.movie = self.listMovie[indexPath.row];
    [self.navigationController pushViewController:playController animated:YES];
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)theCollectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)theIndexPath
{
    
    UICollectionReusableView *theView;
    
    if(kind == UICollectionElementKindSectionHeader)
    {
        theView = [theCollectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:kHeaderCollectionView forIndexPath:theIndexPath];
    } else {
        theView = [theCollectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:kFooterCollectionView forIndexPath:theIndexPath];
        if (!self.loading && (self.listMovie.count > 0)) {
            [self addViewLoadingInFooterView:theView];
        }
    }
    return theView;
}

- (void)addViewLoadingInFooterView:(UICollectionReusableView *)view {
    
    self.loading = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.loading.frame = CGRectMake(self.view.frame.size.width/2 - kSpaceLoadingX, 0, kSpaceLoading, kSpaceLoading);
    [self.loading startAnimating];
    [view addSubview:self.loading];
    
    self.loadingLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.loading.frame.origin.x + self.loading.frame.size.width, 0, kWidthLabelLoadMore, kSpaceLoading)];
    self.loadingLabel.textColor = [ UIColor blackColor];
    self.loadingLabel.textAlignment = NSTextAlignmentLeft;
    self.loadingLabel.text = kLoadMoreData;
    [view addSubview:self.loadingLabel];
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGSize returnSize = CGSizeZero;
    
    if (kDeviceIsPhoneSmallerOrEqual35) returnSize = sizeCellFilmIp5; else if (kDeviceIsPhoneSmallerOrEqual40) returnSize = sizeCellFilmIp5;
    else if (kDeviceIsPhoneSmallerOrEqual47) returnSize = sizeCellFilmIp6; else if (kDeviceIsPhoneSmallerOrEqual55) returnSize = sizeCellFilmIp6Plus;
    else if (kDeviceIpad) returnSize = sizeCellFilmIp6;
    return returnSize;
}

-(void)scrollViewDidScroll: (UIScrollView*)scrollView
{
    float scrollViewHeight = scrollView.frame.size.height;
    float scrollContentSizeHeight = scrollView.contentSize.height;
    float scrollOffset = scrollView.contentOffset.y;
    
    if (scrollOffset + scrollViewHeight == scrollContentSizeHeight)
    {
        //This condition will be true when scrollview will reach to bottom
        if (self.currentPage == self.totalMovie) {
            if (self.loading) {
                [self.loading stopAnimating];
                self.loadingLabel.text = kEmptyString;
            }
        } else {
            if (self.loading) {
                [self.loading startAnimating];
            }
        }
    }
    
}

# pragma Get Movie Delegate
- (void)getMovieAPISuccess:(NSDictionary *)response {
    
    NSMutableArray *indexPaths = [NSMutableArray new];
    NSInteger totalCount = [self.listMovie count];
    
    NSArray *result = [response objectForKey:kResult];
    self.totalMovie = [[response objectForKey:kTotal] integerValue];
    self.currentPage = [[[response objectForKey:kPage] objectForKey:kCurrent] integerValue];
    self.offsetMovie = [[[response objectForKey:kPage] objectForKey:kOffset] integerValue];
    ProgressBarDismissLoading(kEmptyString);
    
    // Add object to current list and refresh
    for (int i = 0; i < result.count; i++) {
        NSDictionary *dictMovie = result[i];
        Movie *newMovie = [Movie initMovieFromJSON:dictMovie];
        [self.listMovie addObject:newMovie];
        [indexPaths addObject:[NSIndexPath indexPathForItem:totalCount + i inSection:0]];
    }
    
    [self.collectionMovie performBatchUpdates:^{
        [self.collectionMovie insertItemsAtIndexPaths:indexPaths];
    } completion:nil];
    
}

- (void)getMovieAPIFail:(NSString *)resultMessage {
    ProgressBarDismissLoading(kEmptyString);
    if ([resultMessage isEqualToString:kTokenExpire]) {
        [Utitlities showAlertTokenExpire:self];
    }
    
    
    [Utitlities showiToastMessage:resultMessage];
}

//*****************************************************************************
#pragma mark -
#pragma mark - ** Handle list movie **

- (void)checkListMovie {
    
    if (self.currentPage < self.totalMovie) {
        NSString *urlTop = [NSString stringWithFormat:self.urlRequest,(int)self.offsetMovie+kPagePlus, [User share].accessToken];
        [self requestTopMovie:urlTop];
    } else {
        ProgressBarDismissLoading(kEmptyString);
    }
}

# pragma Left Menu Delegate
- (void)leftMenuController:(LeftMenuController *)leftController withUrl:(NSString *)url andOffset:(NSInteger)offset {
    [self.listMovie removeAllObjects];
    [self.collectionMovie reloadData];
    self.offsetMovie = offset;
    self.urlRequest = url;
    NSString *urlTop = [NSString stringWithFormat:url, offset, [User share].accessToken];
    ProgressBarShowLoading(kLoading);
    [self requestTopMovie:urlTop];
}

@end
