//
//  PlayCell.h
//  HDOnline
//
//  Created by Bao (Brian) L. LE on 5/12/16.
//  Copyright © 2016 LongBao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlayCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbEpisode;

@end
