//
//  PlayController.h
//  HDOnline
//
//  Created by Bao (Brian) L. LE on 5/10/16.
//  Copyright © 2016 LongBao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlayController : UIViewController

@property (strong, nonatomic) Movie *movie;

@end
