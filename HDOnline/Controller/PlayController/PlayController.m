//
//  PlayController.m
//  HDOnline
//
//  Created by Bao (Brian) L. LE on 5/10/16.
//  Copyright © 2016 LongBao. All rights reserved.
//

#import "PlayController.h"

@interface PlayController () <GetIDMovieDelegate, GetLinkPlayMovieDelegate, UICollectionViewDelegate, UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UIImageView *imgPoster;
@property (weak, nonatomic) IBOutlet UILabel *lbNameVN;
@property (weak, nonatomic) IBOutlet UILabel *lbNameEN;
@property (weak, nonatomic) IBOutlet UILabel *lbInfor;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionEpisode;
@property (weak, nonatomic) IBOutlet UILabel *lbCategory;
@property (weak, nonatomic) IBOutlet UILabel *lbRate;
@property (strong, nonatomic) NSMutableArray *listEpisode;
@property (strong, nonatomic) NSMutableArray *listLink;
@property (strong, nonatomic) NSString *epiNumber;
@property (weak, nonatomic) IBOutlet UIButton *btn320p;
@property (weak, nonatomic) IBOutlet UIButton *btn480p;
@property (weak, nonatomic) IBOutlet UIButton *btn720p;
@property (weak, nonatomic) IBOutlet UIButton *trailer;
@property (weak, nonatomic) IBOutlet UIButton *btnBackup;
@property (weak, nonatomic) IBOutlet UILabel *lbYear;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *csHeightImagePoster;
@property (weak, nonatomic) IBOutlet UILabel *lbImdb;
@property (strong, nonatomic) NSString *linkPlay;

@end

@implementation PlayController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self config];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [DataManager shared].getIDMovieDelegate = self;
    [DataManager shared].getLinkPlayMovieDelegate = self;
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)config {
    self.listEpisode = [[NSMutableArray alloc] init];
    self.listLink = [[NSMutableArray alloc] init];
    [self.collectionEpisode reloadData];
    [self requestLinkMovie];
    [self loadInformationMovie];
    self.epiNumber = @"001";
    self.title = self.movie.nameEN;
    
    // Make corner radius for button
    [self customizeButton:self.btn720p];
    [self customizeButton:self.btn480p];
    [self customizeButton:self.btn320p];
    [self customizeButton:self.btnBackup];
    
    if (kDeviceIpad ) {
        self.csHeightImagePoster.constant +=200;
    } else if (kDeviceIsPhoneSmallerOrEqual40) {
        self.csHeightImagePoster.constant +=10;
    } else if (kDeviceIsPhoneSmallerOrEqual47) {
        self.csHeightImagePoster.constant +=50;
    } else if (kDeviceIsPhoneSmallerOrEqual55) {
        self.csHeightImagePoster.constant +=100;
    }
    
}

- (void)customizeButton:(UIButton *)btn {
    btn.layer.cornerRadius = 5.0;
    btn.layer.masksToBounds = YES;
    btn.layer.borderColor = kBrownColor.CGColor;
    btn.layer.borderWidth = 1.0;
}

- (void)loadInformationMovie {
    self.lbNameEN.text = [NSString stringWithFormat:@"%@ (%@)", self.movie.nameEN, self.movie.year];
    self.lbNameVN.text = [NSString stringWithFormat:@"%@ (%@)", self.movie.nameVN, self.movie.year];
    self.lbRate.text = [Utitlities convertEmptyString:self.movie.filmRate];
    self.lbYear.text = self.movie.year;
    self.lbImdb.text = self.movie.imdb;
    if ([self.movie.updateEpisode isEqualToString:kEmptyString]) {
        self.lbCategory.text = [Utitlities convertEmptyString:self.movie.category];
    } else {
        self.lbCategory.text = [NSString stringWithFormat:@"%@ (Tập %@)", [Utitlities convertEmptyString:self.movie.category], [Utitlities convertEmptyString:self.movie.updateEpisode]];
    }
    
    self.lbInfor.text = [Utitlities convertEmptyString:self.movie.infor];
    NSURLRequest *imageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:self.movie.banner]
                                                  cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                              timeoutInterval:60];
    
    
    [self.imgPoster setImageWithURLRequest:imageRequest
                          placeholderImage:[UIImage imageNamed:kImageDefault]
                                   success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                       self.imgPoster.image = image;
                                   } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                                       self.imgPoster.image = [UIImage imageNamed:kImageDefault];
                                   }];

}

- (void)requestLinkMovie {
    ProgressBarShowLoading(kLoading);
    [[DataManager shared] getRealIDMovieFromUrl:[NSString stringWithFormat:kUrlGetIdMovie, self.movie.movieID, [User share].accessToken]];
}

- (void)disableButton {
    self.btn320p.enabled = NO;
    self.btn480p.enabled = NO;
    self.btn720p.enabled = NO;
    self.btnBackup.enabled = NO;
}

#pragma Get ID Movie Delegate
- (void)getIDMovieAPISuccess:(NSArray *)response {
    ProgressBarDismissLoading(kEmptyString);
    
    NSInteger tmpOrder = 001;
    
    for (NSDictionary *dictResponse in response) {
        Movie *newMovie = [[Movie alloc] init];
        newMovie.movieID = [dictResponse objectForKey:kId];
        newMovie.order = [dictResponse objectForKey:kOrder];
        
        if (self.listEpisode.count == 0) {
            [self.listEpisode addObject:newMovie];
            tmpOrder = [newMovie.order integerValue];
        } else {
            if ([newMovie.order integerValue] > tmpOrder) {
                [self.listEpisode addObject:newMovie];
                tmpOrder = [newMovie.order integerValue];
            } else  {
                
            }
        }
    }
    
    [self.collectionEpisode reloadData];
    
    if ([self.movie.updateEpisode isEqualToString:kEmptyString]) {
        // Phim le
        ProgressBarShowLoading(kLoading);
        Movie *movie = self.listEpisode[0];
        self.epiNumber = @"001";
        [[DataManager shared] requestLinkPlayMovieFromUrl:[NSString stringWithFormat:kUrlPlayPhimLe, movie.movieID, [User share].accessToken]];
        
    } else {
        // Phim bo
        
    }
}

- (void)getIdMovieAPIFail:(NSString *)resultMessage {
    ProgressBarDismissLoading(kEmptyString);
    
    [Utitlities showiToastMessage:resultMessage];
}

#pragma Get Link Play Movie Delegate
- (void)getLinkPlayMovieAPISuccess:(NSArray *)response {
    ProgressBarDismissLoading(kEmptyString);
    
    [self configLinkPlay:response];
    
}

- (void)getLinkPlayMovieAPIFail:(NSString *)resultMessage {
    ProgressBarDismissLoading(kEmptyString);
    if ([resultMessage isEqualToString:kTokenExpire]) {
        [Utitlities showAlertTokenExpire:self];
    }
    
    
    [Utitlities showiToastMessage:resultMessage];
}

#pragma Handle Link Play
- (void)configLinkPlay:(NSArray *)response {
    
    for (NSDictionary *dictResponse in response) {
        NSArray *listSub = [dictResponse objectForKey:kSubtitle];
        NSArray *listLinkPlay = [dictResponse objectForKey:kLevel];
        NSString *link = [dictResponse objectForKey:kLinkPlay];
        
        for (NSDictionary *dictSub in listSub) {
            NSString *linkSubFile = [dictSub objectForKey:kFile];
            NSString *codeSub = [dictSub objectForKey:kCode];
            if ([codeSub isEqualToString:kSubVN] ) {
                self.movie.linkSubtitle = linkSubFile;
                break;
            }
        }
        
        if (link.length > 0) {
            self.btnBackup.enabled = YES;
            self.linkPlay = link;
        }
        
        if (listLinkPlay.count > 0) {
            for (NSDictionary *dictLinkPlay in listLinkPlay) {
                NSString *label = [NSString stringWithFormat:@"%@", [dictLinkPlay objectForKey:kLabel]];
                if ([label isEqualToString:kLink720]) {
                    self.btn720p.enabled = YES;
                } else if ([label isEqualToString:kLink480]) {
                    self.btn480p.enabled = YES;
                } else {
                    self.btn320p.enabled = YES;
                }
                [self.listLink addObject:dictLinkPlay];
            }
        } 
        break;
    }
}

#pragma IBAction
- (IBAction)btn320p:(id)sender {
    [self playMovie:kLink360];
}

- (IBAction)btn480p:(id)sender {
    [self playMovie:kLink480];
}
- (IBAction)btn720p:(id)sender {
    [self playMovie:kLink720];
}
- (IBAction)btnTrailer:(id)sender {
    if (![self.movie.trailer isEqualToString:kEmptyString]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.movie.trailer]];
    }

}
- (IBAction)btnBackup:(id)sender {
    [self playMovie:kLink720];
}

- (void)playMovie:(NSString *)quality {
    
    if (self.listLink.count > 0) {
        for (NSDictionary *dictLinkPlay in self.listLink) {
            NSString *linkPlayMovie = [dictLinkPlay objectForKey:kFile];
            NSString *label = [NSString stringWithFormat:@"%@", [dictLinkPlay objectForKey:kLabel]];
            if ([label isEqualToString:quality]) {
                self.movie.linkPlay = linkPlayMovie;
                [self handlePlayMovie];
                break;
            }
        }
    } else {
        if (self.linkPlay.length > 0) {
            self.movie.linkPlay = self.linkPlay;
            [self handlePlayMovie];

        } else {
            [Utitlities showiToastMessage:kNoLink];
        }
    }
}
    
- (void)handlePlayMovie {
    if (self.movie.linkPlay) {
        ProgressBarShowLoading(kLoading);
        PlayMovieController *playMovieController = [PlayMovieController share];
        NSTimeInterval timePlay = [Utitlities readContentFromFile:[NSString stringWithFormat:@"%@_%@",self.movie.movieID,self.epiNumber]];
        playMovieController.aMovie = self.movie;
        playMovieController.timePlayMovie = timePlay;
        
        NSString *messageForAlert = kEmptyString;
        
        if (self.movie.updateEpisode.length > 0) {
            messageForAlert = [NSString stringWithFormat:kMessageTimePlayForEpi, self.movie.nameVN, (int)self.epiNumber, [Utitlities stringFromTimeInterval:timePlay]];
        } else {
            messageForAlert = [NSString stringWithFormat:kMessageForTimePlay, self.movie.nameVN,[Utitlities stringFromTimeInterval:timePlay]];
        }
        
        if (timePlay > 0) {
            UIAlertController* alert = [UIAlertController
                                        alertControllerWithTitle:kAlertTitle
                                        message:messageForAlert
                                        preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction
                                            actionWithTitle:kWatchMovieFromTime style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction * action) {
                                                playMovieController.timePlayMovie = timePlay;
                                                [playMovieController playMovieWithController:self];
                                            }];
            
            UIAlertAction* cancelAction = [UIAlertAction
                                           actionWithTitle:kWatchMovieFromStart style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction * action) {
                                               playMovieController.timePlayMovie = 0;
                                               [playMovieController playMovieWithController:self];
                                           }];
            
            [alert addAction:defaultAction];
            [alert addAction:cancelAction];
            [self presentViewController:alert animated:YES completion:nil];
        } else {
            playMovieController.timePlayMovie = 0;
            [playMovieController playMovieWithController:self];
        }
    } else {
        [Utitlities showiToastMessage:kNoLink];
    }
}

# pragma UICollection Delegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.listEpisode.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    // Configure the cell
    PlayCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kPlayerCellIdentifier forIndexPath:indexPath];
    Movie *aMovie = self.listEpisode[indexPath.row];
    cell.lbEpisode.text = [NSString stringWithFormat:@"Tập %@", aMovie.order];
    
    if (cell.selected) {
        cell.backgroundColor = kBrownColor;
        cell.lbEpisode.textColor = [UIColor whiteColor];
    } else {
        cell.backgroundColor = kDefaultColor;
        cell.lbEpisode.textColor = [UIColor blackColor];
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    Movie *aMovie = self.listEpisode[indexPath.row];
    self.epiNumber = aMovie.order;
    if ([self.movie.updateEpisode isEqualToString:kEmptyString]) {
        // Phim le
        
    } else {
        [self disableButton];
        ProgressBarShowLoading(kLoading);
        [[DataManager shared] requestLinkPlayMovieFromUrl:[NSString stringWithFormat:kUrlPlayPhimLe, aMovie.movieID, [User share].accessToken]];
        
    }
    
    PlayCell* cell = (PlayCell *)[collectionView  cellForItemAtIndexPath:indexPath];
    cell.lbEpisode.textColor = [UIColor whiteColor];
    cell.backgroundColor = kBrownColor;
    
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath{
    PlayCell* cell = (PlayCell *)[collectionView  cellForItemAtIndexPath:indexPath];
    cell.lbEpisode.textColor = [UIColor blackColor];
    cell.backgroundColor = kDefaultColor;
}

@end
