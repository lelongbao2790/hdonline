//
//  LeftMenuCell.h
//  HDOnline
//
//  Created by Bao (Brian) L. LE on 5/16/16.
//  Copyright © 2016 LongBao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeftMenuCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbName;
@property (weak, nonatomic) IBOutlet UIImageView *imgLeftMenu;

- (void)setInformationLeft:(NSString *)name andNameImage:(NSString *)strImage;

@end
