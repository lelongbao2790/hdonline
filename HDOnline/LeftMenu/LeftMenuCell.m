//
//  LeftMenuCell.m
//  HDOnline
//
//  Created by Bao (Brian) L. LE on 5/16/16.
//  Copyright © 2016 LongBao. All rights reserved.
//

#import "LeftMenuCell.h"

@implementation LeftMenuCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    
    if (selected) {
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = kBrownColor;
        self.lbName.textColor = [UIColor whiteColor];
        self.imgLeftMenu.image = [UIImage imageNamed:kPhimBoMenuIcon];
        [self setSelectedBackgroundView:bgColorView];
    } else {
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = kDefaultColor;
        self.lbName.textColor = [UIColor blackColor];
        self.imgLeftMenu.image = [UIImage imageNamed:kPhimBoMenuIconSelected];
        [self setSelectedBackgroundView:bgColorView];
    }
    
    
}

- (void)setInformationLeft:(NSString *)name andNameImage:(NSString *)strImage {
    self.lbName.text = name;
    self.imgLeftMenu.image = [UIImage imageNamed:strImage];
}

@end
