//
//  LeftMenuController.h
//  HDOnline
//
//  Created by Bao (Brian) L. LE on 5/10/16.
//  Copyright © 2016 LongBao. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LeftMenuDelegate;

@interface LeftMenuController : UIViewController

@property (nonatomic, weak) id<LeftMenuDelegate> delegate;

@end

@protocol LeftMenuDelegate <NSObject>;
- (void)leftMenuController:(LeftMenuController *)leftController
                   withUrl:(NSString *)url
                 andOffset:(NSInteger)offset;

@end;
