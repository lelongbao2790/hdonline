//
//  LeftMenuController.m
//  HDOnline
//
//  Created by Bao (Brian) L. LE on 5/10/16.
//  Copyright © 2016 LongBao. All rights reserved.
//

#import "LeftMenuController.h"

@interface LeftMenuController ()<UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *lbEmail;
@property (weak, nonatomic) IBOutlet UITableView *tbMenu;
@property (strong, nonatomic) NSMutableArray *listDataMenu;

@end

@implementation LeftMenuController

# pragma Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self config];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

# pragma Helper Method
- (void) config {
    
    // Config table view
    self.tbMenu.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tbMenu.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    // Set data for menu
    NSDictionary *dTmp = [[NSDictionary alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"menu" ofType:@"plist"]];
    self.listDataMenu = [[NSMutableArray alloc] init];
    [self.listDataMenu addObjectsFromArray:[dTmp valueForKey:kObjects]];

}

# pragma Table View Delegate
// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.listDataMenu count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LeftMenuCell  *cell = [tableView dequeueReusableCellWithIdentifier:kLeftMenuCellIdentifier];
    if(!cell) { cell = [[LeftMenuCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kLeftMenuCellIdentifier]; }
    
    NSString *name =[[self.listDataMenu objectAtIndex:indexPath.row] valueForKey:kName];
    NSString *nameImage =[[self.listDataMenu objectAtIndex:indexPath.row] valueForKey:kImage];
    [cell setIndentationLevel:[[[self.listDataMenu objectAtIndex:indexPath.row] valueForKey:kLevel] intValue]];
    [cell setInformationLeft:name andNameImage:nameImage];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *url = [[self.listDataMenu objectAtIndex:indexPath.row] valueForKey:kBaseUrlDict];
    NSString *name =[[self.listDataMenu objectAtIndex:indexPath.row] valueForKey:kName];
    
    if ([url isEqualToString:kUrlCountry] ||
        [url isEqualToString:kUrlGetCategory]) {
        // Country and Category
        
        NSString *detailBaseUrl = [[self.listDataMenu objectAtIndex:indexPath.row] valueForKey:kDetailBaseUrl];
        [AppDelegate share].listCategoryController.urlRequest = detailBaseUrl;
        [AppDelegate share].listCategoryController.baseUrl = url;
        [AppDelegate share].listCategoryController.title = name;
        [AppDelegate share].mainPanel.centerPanel = [[NavigationMovieCustomController alloc] initWithRootViewController:[AppDelegate share].listCategoryController];
        
    } else {
        
        // Other option
        [AppDelegate share].mainPanel.centerPanel = [[NavigationMovieCustomController alloc] initWithRootViewController:[AppDelegate share].mainController];
        if ([self.delegate respondsToSelector:@selector(leftMenuController:withUrl:andOffset:)]){
            [self.delegate leftMenuController:self withUrl:url andOffset:0];
        }
    }
    
    [[AppDelegate share].mainPanel showCenterPanelAnimated:YES];
    [[AppDelegate share].window makeKeyAndVisible];    
    
}

@end
