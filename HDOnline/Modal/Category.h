//
//  Category.h
//  HDOnline
//
//  Created by Bao (Brian) L. LE on 5/16/16.
//  Copyright © 2016 LongBao. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Category : NSObject

@property (strong, nonatomic) NSString *idCategory;
@property (strong, nonatomic) NSString *name;

+ (Category *)initCategoryFromJSON: (NSDictionary *)json;

@end
