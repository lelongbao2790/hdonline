//
//  Category.m
//  HDOnline
//
//  Created by Bao (Brian) L. LE on 5/16/16.
//  Copyright © 2016 LongBao. All rights reserved.
//

#import "Category.h"

@implementation Category

+ (Category *)initCategoryFromJSON: (NSDictionary *)json {
    Category *newCategory = [[Category alloc] init];
    newCategory.idCategory = [json objectForKey:kId];
    newCategory.name = [json objectForKey:kName];
    return newCategory;
}

@end
