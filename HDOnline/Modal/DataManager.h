//
//  DataManager.h
//  HDOnline
//
//  Created by Bao (Brian) L. LE on 5/9/16.
//  Copyright © 2016 LongBao. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataManager : NSObject {
    NSObject<LoginDelegate> *loginDelegate;
    NSObject<GetMovieDelegate> *getMovieDelegate;
    NSObject<SearchMovieDelegate> *searchMovieDelegate;
    NSObject<GetIDMovieDelegate> *getIDMovieDelegate;
    NSObject<GetLinkPlayMovieDelegate> *getLinkPlayMovieDelegate;
    NSObject<GetListCategoryDelegate> *getListCategoryDelegate;
}

// Init request operation manager
@property (strong, nonatomic) AFHTTPRequestOperationManager *manager;
@property (strong, nonatomic) AFHTTPRequestOperationManager *managerSearch;

// Delegate
@property (strong, nonatomic) NSObject *loginDelegate;
@property (strong, nonatomic) NSObject *getMovieDelegate;
@property (strong, nonatomic) NSObject *searchMovieDelegate;
@property (strong, nonatomic) NSObject *getIDMovieDelegate;
@property (strong, nonatomic) NSObject *getLinkPlayMovieDelegate;
@property (strong, nonatomic) NSObject *getListCategoryDelegate;
//*****************************************************************************
#pragma mark -
#pragma mark ** Singleton object **
+ (DataManager *)shared;

//*****************************************************************************
#pragma mark -
#pragma mark ** Helper Method **

/*
 * GET TOKEN
 *
 * @param strUrl url string request
 */
- (void)getTokenWithUrl:(NSString *)strUrl;

/*
 * GET LOGIN
 *
 * @param strUrl url string request
 */
- (void)loginWithUrl:(NSString *)strUrl;

/*
 * GET MOVIE
 *
 * @param strUrl url string request
 */
- (void)getMovieFromUrl:(NSString *)strUrl;

/*
 * SEARCH MOVIE
 *
 * @param strUrl url string request
 */
- (void)searchMovieFromUrl:(NSString *)strUrl;

/*
 * GET ID MOVIE
 *
 * @param strUrl url string request
 */
- (void)getRealIDMovieFromUrl:(NSString *)strUrl;

/*
 * REQUEST LINK PLAY MOVIE
 *
 * @param strUrl url string request
 */
- (void)requestLinkPlayMovieFromUrl:(NSString *)strUrl;

/*
 * GET LIST CATEGORY
 *
 * @param strUrl url string request
 */
- (void)getListCategoryFromUrl:(NSString *)strUrl;

@end
