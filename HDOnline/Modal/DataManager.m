//
//  DataManager.m
//  HDOnline
//
//  Created by Bao (Brian) L. LE on 5/9/16.
//  Copyright © 2016 LongBao. All rights reserved.
//

#import "DataManager.h"
const NSInteger kTimeOutIntervalSession = 20;

@implementation DataManager
@synthesize loginDelegate, getMovieDelegate, searchMovieDelegate, getIDMovieDelegate, getLinkPlayMovieDelegate, getListCategoryDelegate;

//*****************************************************************************
#pragma mark -
#pragma mark ** Singleton object **
+(DataManager *)shared
{
    static dispatch_once_t once;
    static DataManager *share;
    dispatch_once(&once, ^{
        share = [[self alloc] init];
    });
    return share;
}

/*
 * Create init method to init base url
 */
- (instancetype)init
{
    self = [super init];
    
    if (self) {
        
        // init manager AFHTTPRequestOperationManager
        self.manager = [self configManagerWithBaseUrl:kBaseMobileUrl];
        self.managerSearch = [self configManagerWithBaseUrl:kBaseWebUrl];
        [self.manager.requestSerializer setValue:@"iPad"
                              forHTTPHeaderField:@"User-Agent"];
        [self.managerSearch.requestSerializer setValue:@"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36" forHTTPHeaderField:@"User-Agent"];
        [self.managerSearch.requestSerializer setValue:kApplicationJson forHTTPHeaderField:kContentType];
        
    }
    
    return self;
}

- (AFHTTPRequestOperationManager *)configManagerWithBaseUrl:(NSString *)baseUrl {
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:baseUrl]];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer setTimeoutInterval:kTimeOutIntervalSession];
    [manager.requestSerializer setValue:@"text/plain"
                          forHTTPHeaderField:kContentType];
    
    return manager;
}

//*****************************************************************************
#pragma mark -
#pragma mark ** Helper Method **

/*
 * GET TOKEN
 *
 * @param strUrl url string request
 */
- (void)getTokenWithUrl:(NSString *)strUrl {
    [self.manager GET:strUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([[responseObject objectForKey:kError] integerValue] == kRequestSuccess) {
            
            // Success
            NSDictionary *result = [responseObject objectForKey:kResult];
            NSString *token = [result objectForKey:kToken];
            [User share].accessToken = token;
            
//            NSString *urlLogin = [NSString stringWithFormat:kUrlLogin, token, [User share].email, [User share].password];
//            [self loginWithUrl:urlLogin];
            
            NSString *urlTop = [NSString stringWithFormat:kUrlTopHDO, 0, [User share].accessToken];
            [[DataManager shared] getMovieFromUrl:urlTop];
            
        } else {
            // Fail
            [loginDelegate loginAPIFail:kLoginFail];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [loginDelegate loginAPIFail:[error localizedDescription]];
    }];
}


/*
 * GET LOGIN
 *
 * @param strUrl url string request
 */
- (void)loginWithUrl:(NSString *)strUrl {
    [self.manager GET:strUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([[responseObject objectForKey:kError] integerValue] == kRequestSuccess) {
            
            // Success
            [loginDelegate loginAPISuccess:[responseObject objectForKey:kResult]];
            
        } else {
            // Fail
            [loginDelegate loginAPIFail:kLoginFail];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [loginDelegate loginAPIFail:[error localizedDescription]];
    }];
}

/*
 * GET MOVIE
 *
 * @param strUrl url string request
 */
- (void)getMovieFromUrl:(NSString *)strUrl {
    [self.manager GET:strUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([[responseObject objectForKey:kError] integerValue] == kRequestSuccess) {
            
            // Success
            [getMovieDelegate getMovieAPISuccess:responseObject];
            
        } else {
            // Fail
            [getMovieDelegate getMovieAPIFail:kTokenExpire];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (errorString(k400BadRequest)) {
            [getMovieDelegate getMovieAPIFail:kServerOverLoad];
        } else if (errorString(k502BadRequest) || errorString(k500BadRequest) ) {
            [getMovieDelegate getMovieAPIFail:kServerOverLoad];
        } else {
            [getMovieDelegate getMovieAPIFail:[error localizedDescription]];
        }
    }];
}

/*
 * SEARCH MOVIE
 *
 * @param strUrl url string request
 */
- (void)searchMovieFromUrl:(NSString *)strUrl {
    strUrl = [strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//    self.managerSearch.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [self.manager GET:strUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([[responseObject objectForKey:kError] integerValue] == kRequestSuccess) {
            
            // Success
            NSLog(@"%@", responseObject);
            NSArray *listMovieSearch = [responseObject objectForKey:kResult];
            if (listMovieSearch.count > 0) {
                [searchMovieDelegate searchMovieAPISuccess:listMovieSearch];
            } else {
                ProgressBarDismissLoading(kEmptyString);
            }
            
        } else {
            // Fail
            [searchMovieDelegate searchMovieAPIFail:kTokenExpire];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (errorString(k400BadRequest)) {
            [searchMovieDelegate searchMovieAPIFail:kServerOverLoad];
        } else if (errorString(k502BadRequest) || errorString(k500BadRequest) ) {
            [searchMovieDelegate searchMovieAPIFail:kServerOverLoad];
        } else {
            [searchMovieDelegate searchMovieAPIFail:[error localizedDescription]];
        }
    }];
}

/*
 * GET ID MOVIE
 *
 * @param strUrl url string request
 */
- (void)getRealIDMovieFromUrl:(NSString *)strUrl {
    [self.manager GET:strUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([[responseObject objectForKey:kError] integerValue] == kRequestSuccess) {
            
            // Success
            NSArray *result = [responseObject objectForKey:kResult];
            
            [getIDMovieDelegate getIDMovieAPISuccess:result];
            
        } else {
            // Fail
            [getIDMovieDelegate getIdMovieAPIFail:kTokenExpire];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (errorString(k400BadRequest)) {
            [getIDMovieDelegate getIdMovieAPIFail:kServerOverLoad];
        } else if (errorString(k502BadRequest) || errorString(k500BadRequest) ) {
            [getIDMovieDelegate getIdMovieAPIFail:kServerOverLoad];
        } else {
            [getIDMovieDelegate getIdMovieAPIFail:[error localizedDescription]];
        }
    }];
}

/*
 * REQUEST LINK PLAY MOVIE
 *
 * @param strUrl url string request
 */
- (void)requestLinkPlayMovieFromUrl:(NSString *)strUrl {
    [self.manager GET:strUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([[responseObject objectForKey:kError] integerValue] == kRequestSuccess) {
            
            // Success
            NSArray *result = [responseObject objectForKey:kResult];
            
            [getLinkPlayMovieDelegate getLinkPlayMovieAPISuccess:result];
            
        } else {
            // Fail
            [getLinkPlayMovieDelegate getLinkPlayMovieAPIFail:kTokenExpire];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (errorString(k400BadRequest)) {
            [getLinkPlayMovieDelegate getLinkPlayMovieAPIFail:kServerOverLoad];
        } else if (errorString(k502BadRequest) || errorString(k500BadRequest) ) {
            [getLinkPlayMovieDelegate getLinkPlayMovieAPIFail:kServerOverLoad];
        } else {
            [getLinkPlayMovieDelegate getLinkPlayMovieAPIFail:[error localizedDescription]];
        }
    }];
}

/*
 * GET LIST CATEGORY
 *
 * @param strUrl url string request
 */
- (void)getListCategoryFromUrl:(NSString *)strUrl {
    [self.manager GET:strUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([[responseObject objectForKey:kError] integerValue] == kRequestSuccess) {
            
            // Success
            NSArray *result = [responseObject objectForKey:kResult];
            
            [getListCategoryDelegate getListCategoryAPISuccess:result];
            
        } else {
            // Fail
            [getListCategoryDelegate getListCategoryAPIFail:kTokenExpire];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (errorString(k400BadRequest)) {
            [getListCategoryDelegate getListCategoryAPIFail:kServerOverLoad];
        } else if (errorString(k502BadRequest) || errorString(k500BadRequest) ) {
            [getListCategoryDelegate getListCategoryAPIFail:kServerOverLoad];
        } else {
            [getListCategoryDelegate getListCategoryAPIFail:[error localizedDescription]];
        }
    }];
}

@end
