//
//  Movie.h
//  HDOnline
//
//  Created by Bao (Brian) L. LE on 5/9/16.
//  Copyright © 2016 LongBao. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Movie : NSObject

@property (strong, nonatomic) NSString *movieID;
@property (strong, nonatomic) NSString *nameVN;
@property (strong, nonatomic) NSString *nameEN;
@property (strong, nonatomic) NSString *poster;
@property (strong, nonatomic) NSString *banner;
@property (strong, nonatomic) NSString *year;
@property (strong, nonatomic) NSString *totalEpisode;
@property (strong, nonatomic) NSString *updateEpisode;
@property (strong, nonatomic) NSString *linkPlay;
@property (strong, nonatomic) NSString *linkSubtitle;
@property (strong, nonatomic) NSString *sequence;
@property (strong, nonatomic) NSString *filmRate;
@property (strong, nonatomic) NSString *imdb;
@property (strong, nonatomic) NSString *category;
@property (strong, nonatomic) NSString *infor;
@property (strong, nonatomic) NSString *order;
@property (strong, nonatomic) NSString *trailer;
+ (Movie *)initMovieFromJSON: (NSDictionary *)json;

+ (NSString *)getLinkImageFromDict:(NSDictionary *)dict;

+ (Movie *)initMovieFromJSONSearch: (NSDictionary *)json;

@end
