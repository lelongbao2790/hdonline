//
//  Movie.m
//  HDOnline
//
//  Created by Bao (Brian) L. LE on 5/9/16.
//  Copyright © 2016 LongBao. All rights reserved.
//

#import "Movie.h"

@implementation Movie

+ (Movie *)initMovieFromJSON: (NSDictionary *)json {
    Movie *newMovie = [[Movie alloc] init];
    newMovie.movieID = [json objectForKey:kId];
    newMovie.nameVN = [json objectForKey:kNameVN];
    newMovie.nameEN = [json objectForKey:kNameEN];
    NSDictionary *dictImageList = [json objectForKey:kImageList];
    newMovie.poster = [Movie getLinkImageFromDict:dictImageList];
    NSString *banner = [dictImageList objectForKey:K900_500];
    if (banner) {
        newMovie.banner = banner;
    } else {
        newMovie.banner = [dictImageList objectForKey:k455_215];
    }
    
    newMovie.year = [NSString stringWithFormat:@"%@",[json objectForKey:kYear]];
    newMovie.totalEpisode = [json objectForKey:kTotalEpisode];
    newMovie.updateEpisode = [json objectForKey:kUpdateEpisode];
    newMovie.filmRate = [json objectForKey:kFilmRate];
    newMovie.sequence = [json objectForKey:kSequence];
    newMovie.category = [json objectForKey:kCategory];
    newMovie.infor = [json objectForKey:kInfor];
    newMovie.trailer = [json objectForKey:kTrailer];
    newMovie.imdb = [NSString stringWithFormat:@"%0.1f",[[json objectForKey:kIMDB] floatValue]];
    return newMovie;
}

+ (NSString *)getLinkImageFromDict:(NSDictionary *)dict  {
    
    NSString *img215215 = [dict objectForKey:k215_215];
    NSString *img215311 = [dict objectForKey:k215_311];
    NSString *img455215 = [dict objectForKey:k455_215];
    if (![img215215 isEqualToString:kEmptyString]) {
        return img215215;
    } else if (![img215311 isEqualToString:kEmptyString]) {
        return img215311;
    } else if (![img455215 isEqualToString:kEmptyString]) {
        return img455215;
    }
    return img215215;
}

+ (Movie *)initMovieFromJSONSearch: (NSDictionary *)json {
    Movie *newMovie = [[Movie alloc] init];
//    newMovie.movieID = [json objectForKey:kId];
//    newMovie.nameVN = [json objectForKey:kNameVN];
//    newMovie.nameEN = [json objectForKey:kNameEN];
//    newMovie.year = [NSString stringWithFormat:@"%d", (int)[[json objectForKey:kYear] integerValue]];
//    
//    NSData *data = [[json objectForKey:kThumb] dataUsingEncoding:NSUTF8StringEncoding];
//    NSDictionary *dictImageList = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//    newMovie.poster = [self getStringUrlPoster:[dictImageList objectForKey:kThumb215311]];
//    newMovie.banner = [self getStringUrlPoster:[dictImageList objectForKey:kThumb455215]];
    
    newMovie.movieID = [json objectForKey:kId];
    newMovie.nameVN = [json objectForKey:kNameVN];
    newMovie.nameEN = [json objectForKey:kNameEN];
    newMovie.imdb = [NSString stringWithFormat:@"%0.1f",[[json objectForKey:kIMDB] floatValue]];
    NSDictionary *dictImageList = [json objectForKey:kImageList];
    newMovie.poster = [dictImageList objectForKey:k215_311];
    NSString *banner = [dictImageList objectForKey:K900_500];
    if (banner) {
        newMovie.banner = banner;
    } else {
        newMovie.banner = [dictImageList objectForKey:k455_215];
    }
    
    newMovie.year = [NSString stringWithFormat:@"%@",[json objectForKey:kYear]];
    newMovie.totalEpisode = [json objectForKey:kTotalEpisode];
    newMovie.updateEpisode = [json objectForKey:kUpdateEpisode];
    newMovie.filmRate = [json objectForKey:kFilmRate];
    NSArray *listCategory = [json objectForKey:kCategory];
    NSString *tmpCategory = kEmptyString;
    for (NSString *categoryStr in listCategory) {
        if (tmpCategory) {
            tmpCategory = [NSString stringWithFormat:@"%@", categoryStr];
        } else {
            tmpCategory = [NSString stringWithFormat:@"%@, %@",tmpCategory, categoryStr];
        }
        
    }
    newMovie.category = tmpCategory;
    newMovie.infor = [json objectForKey:kInfor];
    newMovie.trailer = [json objectForKey:kTrailer];
    
    return newMovie;
}

/*
 * Get string url poster image
 */
+ (nonnull NSString *)getStringUrlPoster:(NSString *)string {
    if ([string containsString:kBaseWebUrl]) {
        return string;
    } else {
        return [NSString stringWithFormat:kBaseImageUrl, string];
    }
}

@end
