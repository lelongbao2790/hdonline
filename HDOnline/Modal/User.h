//
//  User.h
//  HDOnline
//
//  Created by Bao (Brian) L. LE on 5/9/16.
//  Copyright © 2016 LongBao. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

+ (User *)share;

@property (strong, nonatomic) NSString *userId;
@property (strong, nonatomic) NSString *userName;
@property (strong, nonatomic) NSString *password;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *accessToken;

// Init user from json
+ (void)userFromJSON:(NSDictionary *)json;

@end
