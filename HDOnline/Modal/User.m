//
//  User.m
//  HDOnline
//
//  Created by Bao (Brian) L. LE on 5/9/16.
//  Copyright © 2016 LongBao. All rights reserved.
//

#import "User.h"

@implementation User

+ (User *)share {
    static dispatch_once_t once;
    static User *share;
    dispatch_once(&once, ^{
        share = [self new];
    });
    return share;
}

// Init user from json
+ (void)userFromJSON:(NSDictionary *)json {
    NSDictionary *user = [json objectForKey:kUser];
    [User share].accessToken = [json objectForKey:kToken];
    [User share].userId = [user objectForKey:kId];
    [User share].userName = [user objectForKey:kUserName];
    [User share].email = [user objectForKey:KEmail];
    
}

@end
