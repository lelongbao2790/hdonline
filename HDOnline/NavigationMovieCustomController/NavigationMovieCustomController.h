//
//  NavigationMovieCustomController.h
//  HDOnline
//
//  Created by Bao (Brian) L. LE on 5/13/16.
//  Copyright © 2016 LongBao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavigationMovieCustomController : UINavigationController

/*
 * Get child root view controller
 */
- (UIViewController*) getChildRootViewController;

+ (NavigationMovieCustomController *)share;

@end
