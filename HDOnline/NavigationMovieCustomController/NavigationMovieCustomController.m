//
//  NavigationMovieCustomController.m
//  HDOnline
//
//  Created by Bao (Brian) L. LE on 5/13/16.
//  Copyright © 2016 LongBao. All rights reserved.
//

#import "NavigationMovieCustomController.h"

@interface NavigationMovieCustomController ()

@end

@implementation NavigationMovieCustomController

+ (NavigationMovieCustomController *)share {
    static dispatch_once_t once;
    static NavigationMovieCustomController *share;
    dispatch_once(&once, ^{
        share = [self new];
    });
    return share;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [self config];
}

- (void)config {
    [self.navigationBar setBackgroundColor:kBrownColor];
    self.navigationBar.barTintColor = kBrownColor;
    self.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 * Get child root view controller
 */
- (UIViewController*) getChildRootViewController {
    NSArray *s_viewController = @[kPlayControllerStoryBoardID];
    for (NSString *stringViewController in s_viewController) {
        if ([self.topViewController isKindOfClass:NSClassFromString(stringViewController)]) {
            return self.topViewController;
        }
    }
    
    return nil;
}

@end
