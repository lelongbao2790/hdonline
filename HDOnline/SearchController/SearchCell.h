//
//  SearchCell.h
//  HDOnline
//
//  Created by Bao (Brian) L. LE on 5/11/16.
//  Copyright © 2016 LongBao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbNameEN;

@property (weak, nonatomic) IBOutlet UILabel *lbNameVN;
@property (weak, nonatomic) IBOutlet UIImageView *imgPoster;
@property (weak, nonatomic) IBOutlet UILabel *lbYear;
@property (weak, nonatomic) IBOutlet UIView *subView;

- (void) loadInformationWithMovie:(Movie *)movie;

@end
