//
//  SearchCell.m
//  HDOnline
//
//  Created by Bao (Brian) L. LE on 5/11/16.
//  Copyright © 2016 LongBao. All rights reserved.
//

#import "SearchCell.h"

@implementation SearchCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) loadInformationWithMovie:(Movie *)movie {
    // Set text
    self.lbNameEN.text = movie.nameEN;
    self.lbNameVN.text = movie.nameVN;
    self.lbYear.text = movie.year;
    
    // Set image
    NSURLRequest *imageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:movie.poster]
                                                  cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                              timeoutInterval:60];
    
    
    [self.imgPoster setImageWithURLRequest:imageRequest
                          placeholderImage:[UIImage imageNamed:kImageDefault]
                                   success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                       self.imgPoster.image = image;
                                   } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                                       self.imgPoster.image = [UIImage imageNamed:kImageDefault];
                                   }];
    
    // Config border
    self.imgPoster.layer.cornerRadius = 5.0;
    self.imgPoster.layer.masksToBounds = YES;
    self.subView.layer.cornerRadius = 5.0;
    self.subView.layer.masksToBounds = NO;
    self.subView.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    self.subView.layer.shadowOffset = CGSizeMake(3, 3);
    self.subView.layer.shadowOpacity = 1;
    self.subView.layer.shadowRadius = 1.0;
}

@end
