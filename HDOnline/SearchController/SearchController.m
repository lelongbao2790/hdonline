//
//  SearchController.m
//  HDOnline
//
//  Created by Bao (Brian) L. LE on 5/11/16.
//  Copyright © 2016 LongBao. All rights reserved.
//

#import "SearchController.h"

@interface SearchController () <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UISearchControllerDelegate, UISearchResultsUpdating, SearchMovieDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tbSearch;
@property (strong, nonatomic) NSMutableArray *listMovieSearch;
@property (strong, nonatomic) UISearchController *searchController;
@end

@implementation SearchController

# pragma Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self config];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    
    // Delegate search
    [DataManager shared].searchMovieDelegate = self;
}

# pragma Helper Method
- (void) config {
    // Config table view recipe
    self.tbSearch.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tbSearch.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    self.listMovieSearch = [[NSMutableArray alloc] init];
    
    // Init search controller
    self.definesPresentationContext = NO;
    [self initSearchController];
}

/*
 * Init search controller
 */
- (void)initSearchController {
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.searchBar.delegate = self;
    self.searchController.searchBar.placeholder = @"Tìm kiếm phim";
    self.searchController.searchBar.barTintColor = kBrownColor;
    self.searchController.searchBar.tintColor = [UIColor whiteColor];
    self.tbSearch.tableHeaderView = self.searchController.searchBar;
    self.searchController.searchBar.backgroundColor = [UIColor clearColor];
    self.definesPresentationContext = YES;
    [self.searchController.searchBar sizeToFit];
}

# pragma Table View Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.listMovieSearch count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // Init cell
    SearchCell *cell = [tableView dequeueReusableCellWithIdentifier:kSearchCellIdentifier];
    if(!cell) { cell = [[SearchCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kSearchCellIdentifier]; }
    
    [cell loadInformationWithMovie:self.listMovieSearch[indexPath.row]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell     forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) { [tableView setSeparatorInset:UIEdgeInsetsZero]; }
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) { [tableView setLayoutMargins:UIEdgeInsetsZero]; }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) { [cell setLayoutMargins:UIEdgeInsetsZero]; }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    PlayController *playController = InitStoryBoardWithIdentifier(kPlayControllerStoryBoardID);
    playController.movie = self.listMovieSearch[indexPath.row];
    [self.navigationController pushViewController:playController animated:YES];
}

- (void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope
{
    [self updateSearchResultsForSearchController:self.searchController];
}

#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    
    NSString *searchString = searchBar.text;
    
    // Call api search - reload data
    [self requestSearchMovie:searchString];
}

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
   
}

# pragma Search Movie Delegate

- (void)searchMovieAPIFail:(NSString *)resultMessage {
    ProgressBarDismissLoading(kEmptyString);
}

- (void)searchMovieAPISuccess:(NSArray *)response {
    ProgressBarDismissLoading(kEmptyString);
    
    for (NSDictionary *dictItem in response) {
        Movie *newMovie = [Movie initMovieFromJSONSearch:dictItem];
        [self.listMovieSearch addObject:newMovie];
    }
    
    [self.tbSearch reloadData];
    
}

- (void)requestSearchMovie:(NSString *)searchString {
    [self.listMovieSearch removeAllObjects];
    [self.tbSearch reloadData];
    NSString *urlSearch = [NSString stringWithFormat:kUrlSearchMobile, searchString, [User share].accessToken];
    ProgressBarShowLoading(kLoading);
    [[DataManager shared] searchMovieFromUrl:urlSearch];
}

@end
