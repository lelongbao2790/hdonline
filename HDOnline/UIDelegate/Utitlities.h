//
//  Utitlities.h
//  HDOnline
//
//  Created by Bao (Brian) L. LE on 5/9/16.
//  Copyright © 2016 LongBao. All rights reserved.
//

#import <Foundation/Foundation.h>
@class MPMoviePlayerViewController;

@interface Utitlities : NSObject

extern MPMoviePlayerViewController *kMoviePlayer;

/**
 * Show iToast message for informing.
 * @param message
 */
+ (void)showiToastMessage:(nonnull NSString *)message;

/**
 * Convert string
 * @param string
 */
+ (nonnull NSString *)convertEmptyString:(nonnull NSString *)string;

/*
 * Get srt file from url
 */
+ (nonnull NSString*)getDataSubFromUrl:(nonnull NSString*)srtUrl;

+ (void)writeContentToFile:(nonnull NSString *)name andContent:(NSTimeInterval)content;

+ (NSTimeInterval)readContentFromFile:(nonnull NSString *)name;

+ (nonnull NSString *)stringFromTimeInterval:(NSTimeInterval)interval;

// Call this on applicationWillResignActive
+ (void) pauseMovieInBackGround;

// Call this on applicationWillEnterForeground
+ (void) resumeMovieInFrontGround:(nonnull UIViewController *)controller;

+ (void)showAlertTokenExpire:(UIViewController *)controller ;


+ (BOOL)isCrashApp;

+ (void)removeCrashLogFileAtPath:(nonnull NSString *)path;

@end
