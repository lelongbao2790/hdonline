//
//  Utitlities.m
//  HDOnline
//
//  Created by Bao (Brian) L. LE on 5/9/16.
//  Copyright © 2016 LongBao. All rights reserved.
//

#import "Utitlities.h"
#define kTimeDuration 300

@implementation Utitlities
static NSInteger timePlay;
BOOL playbackDurationSet=NO;
MPMoviePlayerController *mediaPlayerController = nil;
MPMoviePlayerViewController *kMoviePlayer = nil;
/**
 * Show iToast message for informing.
 * @param message
 */
+ (void)showiToastMessage:(nonnull NSString *)message {
    iToastSettings *theSettings = [iToastSettings getSharedSettings];
    theSettings.duration = kTimeDuration;
    
    // Prevent crash with null string
    if (message == (id)[NSNull null]) {
        message = kEmptyString;
    }
    
    [[[[iToast makeText:message]
       setGravity:iToastGravityBottom] setDuration:iToastDurationNormal] show];

}

/**
 * Convert string
 * @param string
 */
+ (nonnull NSString *)convertEmptyString:(nonnull NSString *)string {
    if ([string isKindOfClass:[NSNull class]]) {
        return kEmptyString;
    }  else {
        if (string.length > 0) {
            return string;
        } else {
            return kEmptyString;
        }
    }
}

/*
 * Get srt file from url
 */
+ (nonnull NSString*)getDataSubFromUrl:(nonnull NSString*)srtUrl {
    NSString *sub = kEmptyString;
    NSURL  *url = [NSURL URLWithString:srtUrl];
    NSData *urlData = [[NSData alloc] initWithContentsOfURL:url] ;
    if ( urlData )
    {
        sub = [NSString stringWithUTF8String:[urlData bytes]];
    }
    return sub;
}

+ (void)writeContentToFile:(nonnull NSString *)name andContent:(NSTimeInterval)content {
    NSString *timePlayString = [NSString stringWithFormat:@"%ld",(long)content];
    NSString *filePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *fileName = [NSString stringWithFormat:@"/%@.txt",name];
    NSString *fileAtPath = [filePath stringByAppendingString:fileName];
    if (![[NSFileManager defaultManager] fileExistsAtPath:fileAtPath]) {
        [[NSFileManager defaultManager] createFileAtPath:fileAtPath contents:nil attributes:nil];
    }
    [[timePlayString dataUsingEncoding:NSUTF8StringEncoding] writeToFile:fileAtPath atomically:NO];
}

+ (NSTimeInterval)readContentFromFile:(nonnull NSString *)name {
    NSString *filePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *fileName = [NSString stringWithFormat:@"/%@.txt",name];
    NSString *fileAtPath = [filePath stringByAppendingString:fileName];
    NSString *timeString = [[NSString alloc] initWithData:[NSData dataWithContentsOfFile:fileAtPath] encoding:NSUTF8StringEncoding];
    NSTimeInterval timeInterval = [timeString integerValue];
    return timeInterval;
}

+ (nonnull NSString *)stringFromTimeInterval:(NSTimeInterval)interval {
    NSInteger ti = (NSInteger)interval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    if (hours == 0) {
        return [NSString stringWithFormat:@"%02ld:%02ld", (long)minutes, (long)seconds];
    } else {
        return [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds];
    }
    
}

// Call this on applicationWillResignActive
+ (void) pauseMovieInBackGround
{
    if (kMoviePlayer) {
        [kMoviePlayer.moviePlayer pause];
        timePlay = round([kMoviePlayer.moviePlayer currentPlaybackTime]);
        [getChildController dismissMoviePlayerViewControllerAnimated];
    }
}

// Call this on applicationWillEnterForeground
+ (void) resumeMovieInFrontGround:(nonnull UIViewController *)controller
{
    if (kMoviePlayer) {
        NSLog(@"Time resume: %d",(int)timePlay);
        [controller presentMoviePlayerViewControllerAnimated:kMoviePlayer];
        [kMoviePlayer.moviePlayer prepareToPlay];
        [kMoviePlayer.moviePlayer play];
        [kMoviePlayer.moviePlayer setCurrentPlaybackTime:timePlay];
        
    }
}

+ (void)showAlertTokenExpire:(UIViewController *)controller {
    UIAlertController* alert = [UIAlertController
                                alertControllerWithTitle:kErrors
                                message:kTokenExpire
                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction
                                    actionWithTitle:kOK style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kToken];
                                        
                                    }];
    
    [alert addAction:defaultAction];
    [controller.navigationController presentViewController:alert animated:YES completion:nil];
}

+ (BOOL)isCrashApp
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *logPath = [documentsDirectory stringByAppendingPathComponent:kConsoleLog];
    NSData *myData = [NSData dataWithContentsOfFile:logPath];
    
    if (myData) {
        return YES;
    } else {
        return NO;
    }
}

+ (void)removeCrashLogFileAtPath:(nonnull NSString *)path
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSString *filePath = path;
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    if (success) {
        NSLog(@"Remove log success");
    }
    else
    {
        NSLog(@"Can not remove log success");
    }
}

@end
